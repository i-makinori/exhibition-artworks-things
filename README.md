
# 展示会向けの、ポスター・ツールなどの群

展示会向けの、製作物実行プログラムや、ポスターなど。



# launcher

以下のプログラムを呼び出すプログラム。

- 物理シミュレーション。  
  [Makinori Ikegami / physical-worlds · GitLab](https://gitlab.com/i-makinori/physical-worlds)
- 横スクロールゲーム、および、ニューラルネットエージェント。  
  [i-makinori/coin_gather_game: coin gather game which is able to try machine learning algorithms.](https://github.com/i-makinori/coin_gather_game)
- 因数分解プログラム など. (代数系の定理証明プログラムになる予定)  
  [Files · main · Makinori Ikegami / proof-algebra · GitLab](https://gitlab.com/i-makinori/proof-algebra/-/tree/main?ref_type=heads)



# 環境構築

clone Repository

```sh
# git clone https://gitlab.com/i-makinori/exhibition-artworks-things
# cd exhibition-artworks-things/
# git submodule update --init --recursive
```

imagemagick

```sh
# 
# resize image to width 25, keeping aspect ratio
$ convert -geometry 25x src/image1.png out/image1.png
# resize multiple image files (*). need to be called under specific (./) directory.
$ echo 'for f in * ; do convert -geometry 250x "./$f" "./launcher/l_$f"; done' | bash
```


