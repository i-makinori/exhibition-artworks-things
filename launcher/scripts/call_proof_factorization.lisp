
(eval-when (:execute :compile-toplevel :load-toplevel)
  (ql:quickload :proof-algebra))


(in-package :proof-algebra)
(use-package :cl)

;;
(defun format-proov-line-text (line)
  (format t "  上式 の位置~A部 に ~A を適用すると~%  ~A となる。 ~%"
          ;;(prefix->infix (cadddr line))
          ((lambda (part)
             (if (null part) "root" part))
           (caddr line))
          (caadr line)
          (prefix->infix (car line))))

(defun demonstrate-proof (formula-start formula-objective &key (n-search 100000))
  ;; 展示用の、、、探索呼び出しプログラム
  ;; (demonstrate-proof '((a * c) + (c * b) + (d * a) + (b * d)) '((c + d) * (a + b)))
  (let*
      ((searched-path (search-proof (infix->prefix formula-objective)
                                    (infix->prefix formula-start)
                                    *first-axioms*
                                    :n-search n-search)))
    (cond ((eq searched-path +failure+)
           (format t "私は、式を以下のように変換する方法を発見できませんでした:~%  ~A -> ~A~%"
                   formula-start formula-objective)
           +failure+)
          ((listp searched-path)
           (format t "開始式の詳しい中置記法表現には、~A が在る。~%"
                   (prefix->infix (infix->prefix formula-start)))
           (format t "目標式の詳しい中置記法表現には、~A が在る。~%"
                   (prefix->infix (infix->prefix formula-objective)))
           (mapcar #'format-proov-line-text
                   (cdr searched-path))
           (format t "かの如く、~A から ~A への変換は可能。~%"
                   formula-start formula-objective)
           ;;
           searched-path
           )
          (t +failure+))))

(defun quit-symbolp (symbol-test)
  (or (equal symbol-test 'quit) (equal symbol-test 'exit)))

(defun help-formula-to-input ()
  (format t "入力する式の例:~%(x + y)~%(a * b)~%((a + b) * (i + j))~%")
  (format t "(a * (i + j) + b * (j + i))~%など~%")
  (format t "また、 quit を入力することで、プログラムを終了します。~%"))

(defun demonstrate-repl-of-proof (&key (counter 1))
  (let ((text-start)
        (text-objective)
        (loopp t))
    (handler-case
        (progn

          ;; read start-polynomial
          (when t
            (format t "開始式を入力して下さい[~A] >>~%  " counter)
            (setq text-start (read))
            (format t "~A~%" text-start))

          ;; read objective-polynomial
          (when (not (quit-symbolp text-start))
            (format t "目標式を入力して下さい[~A] >>~%  " counter)
            (setq text-objective (read))
            (format t "~A~%" text-objective))

          ;; eval and print
          (cond
            ((or (quit-symbolp text-start) (quit-symbolp text-objective))
             (format t "bye.~%")
             (sleep 2)
             nil ;; quit loop
             ;;(sb-ext:exit)
             (setq loopp nil))
            (t
             (let* ((f1 (identity text-start))
                    (f2 (identity text-objective))
                    (search-result (demonstrate-proof f1 f2)))
               (when (eq search-result +failure+)
                 (help-formula-to-input)))))

          ;; loop
          (if loopp
              (demonstrate-repl-of-proof :counter (1+ counter))))
      
      ;; handle error
      (error (e) e
        (print e)
        (format t "式の認識に失敗しました。~%")
        (format t "再入力して下さい。~%")
        (help-formula-to-input)
        (demonstrate-repl-of-proof :counter (1+ counter))))))


;;;; main

(defun main (args)
  args
  (format t "因数分解 探索プログラム (LENNMA_digress Ver.0.001) ~%")
  (help-formula-to-input)
  (demonstrate-repl-of-proof))
