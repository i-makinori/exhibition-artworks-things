;; #!/usr/bin/sbcl --script

(ql:quickload :physical-worlds)

(in-package :physical-worlds)


(defun main (args)
  (cond
    ((string= args "spring1")
     (test-of-springs-model :viscosity-coefficient 0.0
                            :gravity *gravity-earth*
                            :init-objects-function #'_whole-objects-initial-1_))
    ((string= args "spring2")
     (test-of-springs-model :gravity *gravity-zero* 
                            :init-objects-function #'_whole-objects-initial-matrix-0_))
    ((string= args "quaternion")
     (test-of-handler))
    ((string= args "gravity")
     (test-of-rolling-ball))
    ((string= args "collisions")
     (test-of-collisions))
    (t nil))
  nil)
