#!/usr/bin/bash

# auto relaunch Desktop/launcher.sh

LIVINGP=`ps aux | grep Desktop/launcher.sh | grep -v grep | wc -l`
RELAUNCHP=`ps aux | grep auto-relaunch.sh | grep -v grep | wc -l` 
if [ $LIVINGP -eq 0 ]; then
    echo "launcher is $LIVINGP, Dead."
    urxvt -e ~/Desktop/launcher.sh
fi

exit 1
