#!/usr/bin/python3

import os
try:
    import tkinter as tk
    import tkinter.ttk as ttk
except ImportError:
    import Tkinter as tk
    import Tkinter.ttk as ttk

import subprocess
import shlex


#### application scripts

def call_physical_worlds_scripts (arg_text):
    path = scripts_path("call_physical-worlds-scripts.lisp")
    subprocess.call(shlex.split('ros {0} {1}'.format(path, arg_text)))
    return False

def call_coin_gather_game_game():
    path = scripts_path("coin_gather_game/source/game.py")
    subprocess.call(shlex.split('python3 {0}'.format(path)))
    return False

def call_coin_gather_game_neural():
    path = scripts_path("coin_gather_game/source/neuralnet_ai.py")
    subprocess.call(shlex.split('python3 {0}'.format(path)))
    return False

def call_proof_factorizaton():
    path = scripts_path("call_proof_factorization.lisp")
    arg_text = None
    #subprocess.call(shlex.split('ros {0} {1}'.format(path, arg_text)))
    subprocess.call(shlex.split('urxvt -e ros {0} {1}'.format(path, arg_text)))

#### configs 

root = tk.Tk()
root.title("計算機プログラム集1/(NN)")

#### configs, global dicts

root.tk_setPalette(background='#eeeeee', foreground='black')
#_default_font = ("Helvetica", 22)
#_smaller_font = ("Helvetica", 18)
_default_font = ("Ricty", 22)
_smaller_font = ("Ricty", 18)

widgets={}
images ={} # image_widgets. avoid garbedge collect by tkinter.

#### root and main frames
frame = tk.Frame(root)

frame_head = tk.Frame(root, bg='red')
frame_body = tk.Frame(root, )
frame_foot = tk.Frame(root, bg='green')

frame_head.pack(side='top', pady=10)
frame_body.pack(side='top',)
frame_foot.pack(side='bottom', pady=10)


#### utils

def assets_path(filename):
    return os.path.join(os.path.dirname(__file__), "../assets/", filename)

def scripts_path(rel_path):
    return os.path.join(os.path.dirname(__file__), "./scripts/", rel_path)

def grid_widget(key_text, label_text = True ,function = lambda :None,
                w_type='label',
                image_path=''):

    label_text = key_text if label_text==True else (str(key_text) if label_text!=None else "")

    # image widget
    images[key_text] = None
    asset_path = assets_path(image_path)
    if(image_path!='' and os.path.isfile(asset_path)):
        image_original = tk.PhotoImage(file=asset_path)
        images[key_text]   = image_original.subsample(1, 1)

    # place widgets
    if (w_type == 'label'):
        widgets[key_text] = \
            tk.Label(frame_body, text=label_text,
                     image=images[key_text],
                     font=_default_font)
    elif (w_type == 'button'):
        widgets[key_text] = \
            tk.Button(frame_body, text=label_text,
                      command=function, image=images[key_text],
                      font=_smaller_font, borderwidth="3")
    return widgets[key_text]


def pack_in_col (col, dict_widgets, key_list):
    iter = 0
    for k in key_list:
        dict_widgets[k].grid(column=col, row=iter, padx="5", pady="5",);
        #dict_widgets[k].pack(side = tk.TOP, pady = 5)
        iter+=1
    return True


### widget list


# head frame element
head = tk.Label(frame_head, text="計算機プログラム集1/(NN)", font=_default_font, height=3)
head.pack()


# foot frame element
text_footer= \
'''ボタンをクリックすることで、プログラムを実行できます
作成: 池上 蒔典 <maau3p@gmail.com>'''
foot = tk.Label(frame_foot, text=text_footer, font=_default_font, height=3)
foot.pack()


# body frame elements

grid_widget("Physics1", w_type='button',
            label_text=None, image_path="launcher/l_physical-worlds-rotation-test.png")
grid_widget("物理シミュレーション")
grid_widget("バネモデルI" , w_type='button',
            function = lambda:call_physical_worlds_scripts("spring1"))
grid_widget("バネモデルII", w_type='button',
            function = lambda:call_physical_worlds_scripts("spring2"))
grid_widget("Quaternion による回転", w_type='button',
            function = lambda:call_physical_worlds_scripts("quaternion"))
grid_widget("万有引力", w_type='button',
            function = lambda:call_physical_worlds_scripts("gravity"))
grid_widget("衝突判定", w_type='button',
            function = lambda:call_physical_worlds_scripts("collisions"))


grid_widget("CoinGather1", w_type='button',
            label_text=None, image_path="launcher/l_coin_gather_game.png")
grid_widget("コイン(黒から変色する点)を集める横スクロールゲーム",)
grid_widget("ゲームの実行", w_type='button',
            function = call_coin_gather_game_game)
grid_widget("上記のエージェントをニューラルネット\nにより矯正するプログラム", w_type='button',
            function = call_coin_gather_game_neural)


grid_widget("ProofFactorizaiton", w_type='button', label_text=None, image_path="launcher/l_proof_factorizaiton.png")
grid_widget("因数分解プログラム")
grid_widget("プログラムの実行", w_type='button',
            function = lambda:call_proof_factorizaton())


# layout for body frame
pack_in_col(0, widgets,
            ["Physics1", "物理シミュレーション", "バネモデルI", "バネモデルII", 
             "Quaternion による回転", "万有引力", "衝突判定",])

pack_in_col(1, widgets,
            ["CoinGather1",
             "コイン(黒から変色する点)を集める横スクロールゲーム",
             "ゲームの実行",
             "上記のエージェントをニューラルネット\nにより矯正するプログラム"])

pack_in_col(2, widgets,
            ["ProofFactorizaiton",
             "因数分解プログラム",
             "プログラムの実行"])

                  
# TK main loop
root.mainloop()
